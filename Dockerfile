FROM hypriot/rpi-node

ADD ./ /root/

WORKDIR /root

RUN npm install -g gulp && \
    npm install && \
    gulp pro

EXPOSE 3333

CMD node server/bin/www
