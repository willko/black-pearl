(function (){
  'use strict';

  /**
  ** post.js
  **  expects job.data = { task: 'post', op: ..., id: ..., content: ... }
  **  where:
  **   op is: 'create' || 'delete' || 'update'
  **   id is: the id of the post to modify
  **   content: is the updated message content
  **/

  module.exports = function (job, done) {
    //TODO: handle the post job
    switch(job.data.op){
      case 'create':
        break;
      case 'delete':
        break;
      case 'update':
        break;
      default:
      console.error('[task] Operation not supported: ' + job.data.op);
    }
  };

}());
