(function () {
  'use strict';
  var job_service = job_service || {};

  // sanity check
  if(Object.keys(job_service).length !== 0){
    module.exports = job_service;
    return;
  }

  // setup job handlers (helpers)
  var fs = require('fs');
  var helpers = helpers || {};
  if(Object.keys(helpers).length === 0) { 
    var helper_files = fs.readdirSync(__dirname);
    for(var i = 0; i < helper_files.length; i++)
      if(helper_files[i].indexOf('index.js') === -1)
        helpers[helper_files[i].slice(0, helper_files[i].length - 3)] = 
          {
            'run': require('./' + helper_files[i])
          };
  }

  // delegates the task to process to the specific server-job
  var exec = function (job, done) {
    try{
      helpers[job.data.task].run(job, done);
    }catch (err){
      console.error('[server-jobs] Error: ' + err);
      return err;
    }
  };

  job_service.exec = exec;
  module.exports = job_service;

}());

