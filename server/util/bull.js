(function () {
  'use strict';
  var queue_service = queue_service || {};

  if(Object.keys(queue_service).length !== 0)
    return queue_service;

  var bull = require('bull')
    , inQ = bull('serverQ', '6379', 'redis')
    , outQ = bull('clientQ', '6379', 'redis')
    , handler = require('../server-jobs');


  //listeners
  inQ.process(function (job, done) {
    handler.exec(job, done);
  });

  inQ.on('active', function (){})
       .on('progress', function (){})
       .on('completed', function (){})
       .on('failed', function (){
          //TODO: log failed jobs
       })
       .on('paused', function (){})
       .on('resumed', function (){})
       .on('cleaned', function (){});


  var send = function (data_object) {
    outQ.add(data_object);
  };

  //exported methods
  queue_service.send = send;
  module.exports = queue_service;

}());