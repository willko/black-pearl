#!/usr/bin/env node
module.exports = (function (){
  'use strict';

  var fs = require('fs')
    , path = require('path');

  /**
  	server_data
  		@property: cdnLinks object that holds cdn href/src arrays
  		@property: localLinks object that holds local href/src arrays

  	server_data.cdnLinks
  		@property: css the array of css cdn hrefs
  		@property: js the array of javascript src cdns

  	server_data.localLinks
  		@property: css the array of local css hrefs
  		@property: js the array of local javascript src links
  **/
  var server_data = {};

  // read json data & create resource arrays
  var cdnData = JSON.parse(fs.readFileSync(path.join(__dirname, '../data/cdn-links.json') ));
  var cssFileArray = fs.readdirSync(path.join(__dirname, '../../build/resources/css'));
  var jsFileArray = fs.readdirSync(path.join(__dirname, '../../build/resources/js'));


  // only load cdns if bower is not used
  if (cssFileArray.indexOf('lib.css') === -1)
  	server_data.cdnLinks = (process.env.DEBUG === 'black-pearl') ? cdnData.dev_cdns : cdnData.min_cdns;
  else
  	server_data.cdnLinks = {css: [], js: []};

  var jsIndex = -1;

  // fix href relative paths
  cssFileArray.forEach(addCssBase);
  jsFileArray.forEach(addJsBase);
  server_data.localLinks = { css: cssFileArray, js: jsFileArray};

  if(jsIndex != -1) {
    var tmp, end = jsFileArray.length - 1;
    tmp = jsFileArray[end];
    jsFileArray[end] = jsFileArray[jsIndex];
    jsFileArray[jsIndex] = tmp;
  }

  // Array.forEach() helpers
  function addCssBase (cssFile, index, thisArray){
  	thisArray[index] = 'css/' + cssFile;
  }
  function addJsBase (jsFile, index, thisArray){
    if(thisArray[index] === 'bundle.js') jsIndex = index;
  	thisArray[index] = 'js/' + jsFile;
  }


  return server_data;

}());