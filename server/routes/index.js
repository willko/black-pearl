'use strict';

var express = require('express');
var router = express.Router();
var data = require('../util/server-init');


/* GET root: Render the single page application. */
router.get('/', function(req, res) {
  res.render('_shared/mainView', { title: 'wko',  cdns: data.cdnLinks, local: data.localLinks });
});



module.exports = router;
