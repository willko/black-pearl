(function (){
  'use strict';
  var router = require('express').Router();
  var fs = require('fs');
  var path = require('path');


  router.get('/', function (req, res){
    res.send('route is working for data');
  });

  router.get('/msg-posts', function (req, res) {
    var data;
    try{
      data = fs.readFileSync(path.join(__dirname, '/../data/msg-posts.json'));
      data = JSON.parse(data);
    }catch(err){
      console.error('[router-error] Problem reading the file:  msg-posts');
      data = JSON.parse('{}');
    }
    res.send(data);
  });


  module.exports = router;

}());