(function () {
'use strict';

//node_modules
var gulp = require('gulp')
  , sourcemaps = require('gulp-sourcemaps')
  , browserify = require('gulp-browserify')
  , ngAnnotate = require('gulp-ng-annotate')
  , babel = require('gulp-babel')
  , filter = require('gulp-filter')
  , jshint = require('gulp-jshint')
  , lreload = require('gulp-livereload')
  , order = require('gulp-order')
  , stylish = require('jshint-stylish')
  , stylus = require('gulp-stylus')
  , uglify = require('gulp-uglify')
  , mainBowerFiles = require('main-bower-files')
  , shell = require('shelljs')
  , concat = require('gulp-concat');

// build data
var paths = {
  js: {
        src: 'client/**/*js'
      , dest: 'build/resources/js'
      , tmp: 'build/_tmp'
      , entry_point: 'build/_tmp/modules/app.modules.js'
      , exit_point: 'bundle.js'
      , server: 'server/**/*js'
      , lib: {
          min: [
            'bower_components/*min.js'
          , 'bower_components/**/*min.js'
        ]
        , exit_point: 'lib.js'
      }
  }, 
  styles: {
        src: 'client/modules/**/*styl'
      , dest: 'build/resources/css'
      , exit_point: 'styles.css'
      , lib: {
          min: [
            'bower_components/*min.css'
          , 'bower_components/**/*min.css'
        ]
        , dest: 'build/resources/css'
        , exit_point: 'lib.css'
      }
  }, 
  views: {
      src: 'client/modules/**/*jade'
    , dest: 'build/views'
    , base: 'client/modules'
  }
};

// --- js ----------------------------------------------------- //


gulp.task('dev_bundle', ['bundle'], function (){
  shell.exec('rm -r ' + paths.js.tmp);
});
gulp.task('min_bundle', ['bundle_min'], function (){
  shell.exec('rm -r ' + paths.js.tmp);
});



gulp.task('lint', function() {
  return gulp.src(paths.js.src)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});
gulp.task('lint_server', function() {
  return gulp.src(paths.js.server)
    .pipe(jshint())
    .pipe(jshint.reporter(stylish));
});



gulp.task('bundle', ['annotate'], function () {
  return gulp.src(paths.js.entry_point)
      .pipe(browserify())
      .pipe(concat(paths.js.exit_point))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(paths.js.dest));
});
gulp.task('bundle_min', ['annotate'], function () {
  return gulp.src(paths.js.entry_point)
      .pipe(browserify())
      .pipe(concat(paths.js.exit_point))
      .pipe(uglify())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(paths.js.dest));
});


gulp.task('annotate', ['babel'], function () {
  return gulp.src('build/_tmp/**/*.js')
      .pipe(ngAnnotate())
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(paths.js.tmp));
});

gulp.task('babel', function () {
  shell.exec('mkdir -p ' + paths.js.tmp);
  return gulp.src(paths.js.src)
        .pipe(sourcemaps.init())
        .pipe(babel())
        .pipe(gulp.dest('build/_tmp'));
});

// called manually from host_libs
gulp.task('lib_js', function () {
  var jsFilter = filter(['*/**/*.js', '*.js']);
  console.log('hello');
  return gulp.src(mainBowerFiles(), { base: './bower_components' })
      .pipe(jsFilter)
      .pipe(order(['*/**/*jquery*', '*/**/*angular.js', '*/**/*animate*', '*/**/*aria*', '*/**/*', '*']))
      .pipe(concat(paths.js.lib.exit_point))
      //.pipe(uglify())
      //.pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(paths.js.dest));
});

gulp.task('clean_js', function (){
  shell.exec('mkdir -p ' + paths.js.dest);
  shell.exec('rm -r ' + paths.js.dest);
});

// --- jade --------------------------------------------------- //

gulp.task('jade', function (){
  return gulp.src(paths.views.src, {base: paths.views.base})
    .pipe(gulp.dest(paths.views.dest))
    .pipe(lreload());
});

//called manually if needed
gulp.task('clean_views', function (){
  shell.exec('mkdir -p ' + paths.views.dest);
  shell.exec('rm -r ' + paths.views.dest);
});

// --- css ---------------------------------------------------- //

gulp.task('stylus', function () {
  gulp.src(paths.styles.src)
    .pipe(stylus())
    .pipe(concat(paths.styles.exit_point))
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(lreload());
});

// called manually from host_libs
gulp.task('lib_css', function () {
  gulp.src(paths.styles.lib.min)
    .pipe(concat(paths.styles.lib.exit_point))
    .pipe(gulp.dest(paths.styles.lib.dest))
    .pipe(lreload());
});

gulp.task('clean_css', function (){
  shell.exec('mkdir -p ' + paths.styles.dest);
  shell.exec('rm -r ' + paths.styles.dest);
});

// --- watch -------------------------------------------------- //

gulp.task('watch', function() {
  lreload.listen();
  gulp.watch(['gulpfile.js', paths.views.src], ['jade']);
  gulp.watch(['gulpfile.js', paths.js.src], ['lint', 'dev_bundle']);
  gulp.watch(['gulpfile.js', paths.styles.src], ['stylus', 'lib_css']);
  gulp.watch(['gulpfile.js', paths.js.server], ['lint_server']);
});

// --- main:
gulp.task('clean', ['clean_js', 'clean_views', 'clean_css']);
gulp.task('host_libs', ['lib_js', 'lib_css']); // requires bower install
gulp.task('dev', ['watch', 'dev_bundle', 'lint', 'lint_server', 'jade', 'stylus']);
gulp.task('pro', ['min_bundle', 'lint', 'lint_server', 'jade', 'stylus']);

}());
