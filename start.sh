#!/bin/bash

docker rm black-pearl ;

docker build -t willko/black-pearl . ;

docker run -d --name black-pearl -p 3333:3333 -v `pwd`:/app willko/black-pearl ;

exit;