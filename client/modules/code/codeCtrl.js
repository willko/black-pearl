(function () {
  'use strict';

  /**
  ** code
  **  This module tests out es6 and ngdocs!
  **/

  //@ngInject
  module.exports = ($scope, $rootScope) => {
    console.log('Using codeCtrl!!');
    $rootScope.helloMsg = 'Code 2!!';
  };

}());
