module.exports = function (){
'use strict';

//@ngInject
return function($stateProvider, $urlRouterProvider){

  $urlRouterProvider.otherwise('/404');
  $urlRouterProvider.when('', '/');

  $stateProvider
    .state('404', {
      url: '/404',
      templateUrl: 'views/_shared/404.html'
    })
    .state('root', {
      url: '/',
      templateUrl: 'views/home/homeView.html',
      controller: 'homeCtrl',
      views: {
        'subHome@' : {
          templateUrl: 'views/home/partials/homeContentView.html'
        }
      }
    })
    .state('code', {
      url: '/code',
      templateUrl: 'views/code/codeView.html',
      controller: 'codeCtrl'
    })
    .state('about', {
      url: '/about',
      templateUrl: 'views/about/aboutView.html',
      controller: 'aboutCtrl'
    });
  };


}();