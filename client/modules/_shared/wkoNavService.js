(function (){
  'use strict';

  //@ngInject
  module.exports = function(){
    var this_service = {};

    this_service.nav = { 
      lock: false,
      openLeftMenu: function (){
        this_service.nav.lock = !(this_service.nav.lock);
      }
    };

    return  this_service;
  };

}());
