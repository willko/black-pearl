(function(){
'use strict';

  //the main site module
  var wko = angular.module('wko', [
    'ui.router',
    'ngMaterial'
  ]);

  //ui-router setup
  wko.config(['$stateProvider', '$urlRouterProvider', require('./app.states.js')]);

  //site services
  wko.factory('wkoNavService', require('./_shared/wkoNavService'));

  //site controllers
  wko.controller('mainCtrl', require('./_shared/mainCtrl'))
     .controller('homeCtrl', require('./home/homeCtrl'))
     .controller('codeCtrl', require('./code/codeCtrl'))
     .controller('aboutCtrl', require('./about/aboutCtrl'));

}());
